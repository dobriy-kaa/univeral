import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeView } from './home/home-view.component';
import { CatsView } from './cats/cats-view.component';
import { SwagView } from './swag/swag-view.component';
import { DabView } from './dab/dab-view.component';
import { TestService } from '../services/test';
import {MdButtonModule, MatToolbarModule} from '@angular/material';
import {LazyImg} from './directives/LazyImg';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    MdButtonModule,
    MatToolbarModule,
    RouterModule.forRoot([
      { path: '', component: HomeView, pathMatch: 'full'},
      { path: 'cats', component: CatsView, pathMatch: 'full'},
      { path: 'dab', component: DabView, pathMatch: 'full'},
      { path: 'swag', component: SwagView, pathMatch: 'full'}
    ])
  ],
  declarations: [
    AppComponent,
    HomeView,
    CatsView,
    SwagView,
    DabView,
    LazyImg
  ],
  exports: [ AppComponent ],
  providers: [
    TestService
  ]
})
export class AppModule {}
