import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransferState } from '../../modules/transfer-state/transfer-state';
import { TestService } from '../../services/test';

@Component({
  selector: 'swag-view',
  templateUrl: './swag.template.html',
  styleUrls: ['./swag.component.scss']
})
export class SwagView implements OnInit {
  public swagCols: any;

  constructor(
    private cache: TransferState,
    private testService: TestService
  ) {
    console.log('!swag-view construct!')
  }

  ngOnInit() {
    if (this.cache.get('swag')) {
      this.swagCols = this.cache.get('swag');
    } else {
      this.getSwag()
    }
  }

  getSwag() {
    this.testService.getGifs('swag').subscribe(res => {
      this.swagCols = TestService.makeCols(res.data)
      this.cache.set('swag', this.swagCols);
    });
  }
}
