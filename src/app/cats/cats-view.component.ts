import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransferState } from '../../modules/transfer-state/transfer-state';
import { TestService } from '../../services/test';

@Component({
  selector: 'cats-view',
  templateUrl: './cats.template.html',
  styleUrls: ['./cats-view.component.scss']
})
export class CatsView implements OnInit {
  public message: string;
  public catsCols: any;

  constructor(
    private cache: TransferState,
    private testService: TestService
  ) {
    console.log('!cats-view construct!')
  }

  getCats() {
    this.testService.getGifs('cats').subscribe(res => {
      this.catsCols = TestService.makeCols(res.data)
      this.cache.set('cats', this.catsCols);
    });
  }

  ngOnInit() {
    if (this.cache.get('cats')) {
      this.catsCols = this.cache.get('cats');
    } else {
      this.getCats();
    }
  }
}
