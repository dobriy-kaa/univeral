import {Directive, ElementRef, Renderer2, Input, PLATFORM_ID, Inject} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Directive({
  selector: "[lazyImg]"
})
export class LazyImg {
  @Input() image: string = '';
  isLoaded: boolean = false;

  constructor(
      private el: ElementRef,
      private renderer: Renderer2,
      @Inject(PLATFORM_ID) private platformId: string
  ) {

  }

  ngOnInit() {
    if (!isPlatformBrowser(this.platformId)) {
      return false;
    } else {
      //навешиваем на window onscroll проверочку, что если дошли, то грузим картинкус
      window.addEventListener('scroll', this.onScroll.bind(this));
      this.onScroll();
    }
  }

  onScroll() {
    if (this.isLoaded) {
      return false;
    }
    const el = this.el.nativeElement;
    const elRect = el.getBoundingClientRect();
    if (elRect.top < window.innerHeight) {
      el.src = this.image;
      this.isLoaded = true;
    }
  }
}
