import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransferState } from '../../modules/transfer-state/transfer-state';
import { TestService } from '../../services/test';

@Component({
  selector: 'dab-view',
  templateUrl: './dab.template.html',
  styleUrls: ['./dab-view.component.scss']
})
export class DabView implements OnInit {
  public dabCols: any;

  constructor(
    private cache: TransferState,
    private testService: TestService
  ) {
    console.log('!dab-view construct!')
  }

  ngOnInit() {
    if (this.cache.get('dab')) {
      this.dabCols = this.cache.get('dab');
    } else {
      this.getDab();
    }
  }

  getDab() {
    this.testService.getGifs('dab').subscribe(res => {
      this.dabCols = TestService.makeCols(res.data)
      this.cache.set('dab', this.dabCols);
    });
  }
}
