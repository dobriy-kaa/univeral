import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransferState } from '../../modules/transfer-state/transfer-state';
import { TestService } from '../../services/test';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'home-view',
  templateUrl: './home.template.html'
})
export class HomeView implements OnInit {
  public message: string;
  public gif: string;
  public isClient: boolean = false;

  constructor(
    private cache: TransferState,
    private testService: TestService,
    @Inject(PLATFORM_ID) private platformId: string
  ) {
    console.log('!home-view construct!')
    this.isClient = isPlatformBrowser(this.platformId);
  }

  ngOnInit() {
    if (this.cache.get('gif')) {
      this.gif = this.cache.get('gif');
    } else {
      this.getRandomGif();
    }
  }

  getRandomGif() {
    this.testService.getRandomGif().subscribe(res => {
      this.gif = res.data.image_original_url;
      this.cache.set('gif', res.data.image_original_url);
    });
  }
}
