import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Observable'
import _ from 'lodash';

import 'rxjs/add/operator/map';

@Injectable()
export class TestService {
  private apiKey:string = '0d596670eb594325a10002703cb9ad33';

  static makeCols(source) {
    const count = 3;
    let inc = 0;
    const cols = [];
    for (let i = 0; i < source.length; i++) {
      cols[inc] = cols[inc] || [];
      cols[inc].push(source[i]);
      inc++;
      if (inc >= count) {
        inc = 0;
      }
    }

    return cols;
  }

  constructor(
    private http: Http
  ) {

  }

  getRandomGif() {
    return this.http
      .get('http://api.giphy.com/v1/gifs/random', {params: {api_key: this.apiKey, rating: 'r'}})
      .map(res => res.json())
  }

  getGifs(keyword = 'poop') {
    const offset = Math.floor(Math.random() * (666 - 0)) + 0;
    return this.http
      .get('http://api.giphy.com/v1/gifs/search',
        {params:
          {
            api_key: this.apiKey,
            q: keyword,
            limit: 100,
            offset
          }
        }
      )
      .map(res => res.json())
  }
}
